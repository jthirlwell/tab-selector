(function ($) {
    //Your plugin's name
    var pluginName = 'tabSelector';
    
    $.fn.extend({
        'tabSelector': function (options) {
            var defaults = {
                //Default values for the plugin's options here
            };

            options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;
                var tabSelector = $(this);
                
                //setup
                
                //Add enabled class
                $(this).addClass('tab-selector-enabled')
               
                //Get tab container
                var tabsContainer = $(this).find('.tab-selector__tabs');
                
                //Get tab content 
                var tabSelectorContent = $('.tab-selector__tab-content');
                
                //Set default content states    
                $(tabSelectorContent).slice(1).attr('aria-hidden', 'true');
                
                //Set first tab to have aria-selected attribute
                $(tabsContainer).find('li:first a').attr('aria-selected', 'true');  
                
                //First tab content to be visible
                $(tabSelectorContent[0]).attr('aria-hidden', 'false');  
                
                //Tab links
                $(tabsContainer).find('a').each( function(){
                    var tabLink = $(this);
               
                    //Bind click event
                    tabLink.click(function(element){
                        
                        //Remove selected tab state
                        $(tabsContainer.find("a[aria-selected='true']").attr('aria-selected', 'false'));
                        
                        //Add selected tab state
                        $(tabLink.attr('aria-selected', 'true'));
                      
                        //Hide content
                        if(tabSelectorContent.is(":visible")){
                           $(tabSelectorContent).attr('aria-hidden', 'true');
                        }
                        //Show content 
                        var contentTabId = $(this).attr('href'); 
                        $(contentTabId).attr('aria-hidden', 'false');
                        
                        element.preventDefault();
                    });
                    
                });

            });
        }
    });
})(jQuery);

