#Tab Selector

A simple, accessible, tab selector

##Features

- Accessible. Content can be accessed without JavaScript. Use of ARIA and Role attributes.
- Progressive enhancement (CSS3 transisition to fade-in content)
- jQuery plugin

##Future improvements & nice to haves

- Improve responsive - if there are more than x tabs, perhaps have tabs as a accordion on mobile, standard tabs for tablet and desktop breakpoints
- IE 6 & 7 support - attribute CSS selectors currently limit functionality for these browsers - so classes would need to be added by jQuery, and add styling for these
- Re-write without the need for jQuery - can be tricky for older browsers. 
- Remember tab position - when navigating back to the page, the tab postion could persist 
- Additional keyboard navigation - perhaps implementing cursor keys & and focus state on content when selecting a tab

##Libraries

- jQuery (1.11.x for IE8 support)
- Jasmine (tests)

##Usage

    $(function(){      
        $('.tab-selector').tabSelector();
    });   

##Example mark up 

    <div class="tab-selector">
        <ul class="tab-selector__tabs">
            <li>
                <a href="#tab-content-first" title="Content of first tab" id="tab-first" role="tab" aria-controls="Item One">Item One</a>
            </li>
            <li>
                <a href="#tab-content-second" title="Content of second tab" id="tab-second" role="tab" aria-controls="Item Two">Item Two</a>
            </li>
            <li>
                <a href="#tab-content-third" title="Content of third tab" id="tab-third" role="tab" aria-controls="Item Three">Item Three</a>
            </li>
        </ul>
         <div id="tab-content-first" name="tab-content-first" class="tab-selector__tab-content" aria-labelledby="tab-first" role="tabcontent">
            <p>First tab content</p>
        </div>
        <div id="tab-content-second" name="tab-content-second" class="tab-selector__tab-content" labelledby="tab-second" role="tabcontent">
            <p>Second tab content</p> 
        </div>
        <div id="tab-content-third" name="tab-content-third" class="tab-selector__tab-content" labelledby="tab-third" role="tabcontent">
            <p>Third tab content</p>
        </div>
    </div>

##Browser support

-IE8+, Chrome, Safari, Firefox, mobile Webkit/Safari browsers

##Demo

[Demo](https://dl.dropboxusercontent.com/u/1470974/projects/tab-selector/demo.html)

##Tests

[Tests](https://dl.dropboxusercontent.com/u/1470974/projects/tab-selector/spec/specRunner.html)