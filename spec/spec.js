jasmine.getFixtures().fixturesPath = 'fixtures';
var fixture;

 beforeEach(function () {
      jasmine.getFixtures().clearCache();
      loadFixtures('tab-selector.html');
      fixture = $('.tab-selector').tabSelector();
    });

describe("the required markup", function(){
    it("should have the tab container class", function(){
        expect($('.tab-selector__tabs')[0]).toBeInDOM()
       
    });
    it("should have the content container class", function(){
         expect($('.tab-selector__tab-content')[0]).toBeInDOM()
    });
    it("should have three tabs and three content containers", function(){
         expect($('.tab-selector__tabs li a')).toHaveLength(3);
         expect($('.tab-selector__tab-content')).toHaveLength(3);
    });
    
});

describe("default tab state", function() {      
    it("should have an aria-selected attribute on the first tab", function(){
        expect( $('.tab-selector__tabs li:first a') ).toHaveAttr('aria-selected', 'true');
    })
    
    it("should have an aria-visible attribute on the first tab's content", function(){
        expect( $('.tab-selector__tab-content:first') ).toHaveAttr('aria-hidden', 'false');
    })
    
    it("should show the first tab's content", function(){
        expect( $('.tab-selector__tab-content:first') ).toBeVisible();
    }) 
});

describe("clicking on the second tab", function(){
        var tabLink = '.tab-selector__tabs li:eq(1) a';
    
        var clickEvent = function(){  
          
            var spyEvent = spyOnEvent(tabLink, 'click')
            $(tabLink).click()
            expect('click').toHaveBeenTriggeredOn(tabLink)
            expect(spyEvent).toHaveBeenTriggered()                    
                              
        };
    
    
        it("should have an aria-selected attribute on the second tab", function(){
            clickEvent();
            expect( $(tabLink) ).toHaveAttr('aria-selected', 'true');
        })
        
        it("should show the second tab's content", function(){
            clickEvent();
            expect( $('.tab-selector__tab-content:eq(1)') ).toBeVisible();

        })
    
        
         
         
});

describe("clicking on the third tab", function(){
    
        var tabLink = '.tab-selector__tabs li:eq(2) a';
    
        var clickEvent = function(){  
            var spyEvent = spyOnEvent(tabLink, 'click')
            $(tabLink).click()
            expect('click').toHaveBeenTriggeredOn(tabLink)
            expect(spyEvent).toHaveBeenTriggered()                    
                              
        };
    
        it("should have an aria-selected attribute on the third tab", function(){
            clickEvent();
            expect( $(tabLink) ).toHaveAttr('aria-selected', 'true');
        })
    
        it("should show the third tab's content", function(){
            clickEvent();
            expect( $('.tab-selector__tab-content:eq(2)') ).toBeVisible();

        });
         
         
});

describe("clicking on the first tab", function(){
    
        var tabLink = '.tab-selector__tabs li:eq(0) a';
        
        var clickEvent = function(){  
            var spyEvent = spyOnEvent(tabLink, 'click')
            $(tabLink).click()
            expect('click').toHaveBeenTriggeredOn(tabLink)
            expect(spyEvent).toHaveBeenTriggered()                    
                              
        };
    
        it("should have an aria-visible attribute on the first tab's content", function(){
            clickEvent();
            expect( $('.tab-selector__tab-content:first') ).toHaveAttr('aria-hidden', 'false');
        })
         
        it("should show the first tab's content", function(){
            clickEvent();
            expect( $('.tab-selector__tab-content:eq(0)') ).toBeVisible();

        });
         
         
});